FROM adoptopenjdk:11-jre-hotspot
MAINTAINER avantcab.com
ARG JAR_FILE=target/apps-rest-vtc-register.jar
EXPOSE 8080
ADD ${JAR_FILE} /app/appsrestvtcregister.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar","/app/appsrestvtcregister.jar", "com/avantcab/appsrestvtcregister/AppsRestVtcRegisterApplication.java"]
