CREATE TABLE vtc_register.service_id_vtc_register_id
(
    service_id bigint PRIMARY KEY,
    vtc_register_id  bigint NOT NULL,
    status varchar(200)
);
