package com.avantcab.appsrestvtcregister.service;

import com.avantcab.appsrestvtcregister.model.entity.PointCodeZone;
import com.avantcab.appsrestvtcregister.repository.PointCodeZoneRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PointCodeZoneService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PointCodeZoneService.class);
    
    private final PointCodeZoneRepository pointCodeZoneRepository;
    
    public String updateAllPointCodeZone(final List<PointCodeZone> pointCodeZones) {
        LOGGER.info("Updating point codes...");
        
        final List<PointCodeZone> pointCodeZoneList = pointCodeZones.stream()
            .filter(pointCodeZone -> !pointCodeZone.getZone().isBlank())
            .collect(Collectors.toList());
        
        pointCodeZoneRepository.deleteAll();
        pointCodeZoneRepository.saveAll(pointCodeZoneList);
        
        return "Actualizado correctamente los puntos de recogida!";
    }
    
}
