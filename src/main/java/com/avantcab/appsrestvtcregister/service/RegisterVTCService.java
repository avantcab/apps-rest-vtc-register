package com.avantcab.appsrestvtcregister.service;

import com.avantcab.appsrestvtcregister.model.TransferServiceData;
import com.avantcab.appsrestvtcregister.model.VtcTransfer;
import com.avantcab.appsrestvtcregister.model.xml.InvoicedServices;
import com.avantcab.appsrestvtcregister.model.xml.TransferService;
import com.avantcab.appsrestvtcregister.repository.VehicleRepository;
import com.avantcab.appsrestvtcregister.utils.DateFormatUtils;
import com.avantcab.appsrestvtcregister.utils.JAXBConversionUtils;
import com.avantcab.appsrestvtcregister.webserviceclient.mind2cloud.LoginClient;
import com.avantcab.appsrestvtcregister.webserviceclient.mind2cloud.PrivatesClient;
import com.avantcab.appsrestvtcregister.webserviceclient.vtc.RegisterVTCClient;
import com.vtcregister.client.ABODYTYPERES;
import com.vtcregister.client.EVTCSERVICIOEMPRE;
import com.vtcregister.client.SOAPException_Exception;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.avantcab.appsrestvtcregister.utils.DateFormatUtils.DATE_TIME_FORMATTER;
import static com.avantcab.appsrestvtcregister.utils.DateFormatUtils.LOCAL_DATE_PATTERN;
import static com.avantcab.appsrestvtcregister.utils.DateFormatUtils.ZONE_ID_PARIS;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RegisterVTCService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterVTCService.class);
    private static final String DEFAULT_AIRPORT = "PMI";
    
    private final ConversionService conversionService;
    private final RegisterVTCClient registerVTCClient;
    
    private final JAXBConversionUtils jaxbConversionUtils;
    private final VehicleRepository vehicleRepository;
    private final PrivatesClient privatesClient;
    private final LoginClient loginClient;
    
    public List<VtcTransfer> getPendingTransfersToRegister(final String date) {
        final String idSession = this.loginClient.getIdSession();
        
        final XMLGregorianCalendar parsedDate = DateFormatUtils.convertDateToXMLGregorianCalendar(
            date.isEmpty() ? getTodayDate() : date,
            LOCAL_DATE_PATTERN
        );
        
        final String serviceResponseXML = privatesClient.getPrivates(idSession, parsedDate);
        final InvoicedServices invoicedServices = jaxbConversionUtils.convertStringXMLToObject(serviceResponseXML, InvoicedServices.class);
        
        final List<Integer> allDriverIds = vehicleRepository.getAllVehicleIds();
        final List<TransferService> transferServices = invoicedServices.getTransferServices().stream()
            .filter(transferService -> transferService.getServiceAirportType().getAirportCode().equals(DEFAULT_AIRPORT))
            .filter(transferService -> allDriverIds.contains(transferService.getVehicleId()))
            .filter(transferService -> {
                final LocalDateTime startServiceDateTime = transferService.getServiceDate()
                    .atTime(transferService.getServiceTime());
                
                return startServiceDateTime.isAfter(getCurrentDateTimeWith3HoursMinus());
            })
            .map(transferService -> {
                final TransferService.TransferServiceBuilder transferServiceToBuilder = transferService.toBuilder();
                final LocalDateTime currentDateTime = LocalDateTime.now(ZONE_ID_PARIS);
                final LocalDateTime startServiceDateTime = transferService.getServiceDate()
                    .atTime(transferService.getServiceTime());
                
                if (startServiceDateTime.isAfter(getCurrentDateTimeWith3HoursMinus()) &&
                    startServiceDateTime.isBefore(currentDateTime)) {
                    final LocalDateTime localDateTime = currentDateTime.plusMinutes(5);
                    transferServiceToBuilder.serviceTime(localDateTime.toLocalTime());
                }
                
                return transferServiceToBuilder.build();
            })
            .collect(Collectors.toList());
        
        return transferServices.stream()
            .map(transferService -> this.conversionService.convert(transferService, VtcTransfer.class))
            .collect(Collectors.toList());
    }
    
    public List<VtcTransfer> getPendingTransfersToRegisterAuro(final String date) {
        final String idSession = this.loginClient.getIdSession();
        
        final XMLGregorianCalendar parsedDate = DateFormatUtils.convertDateToXMLGregorianCalendar(
            date.isEmpty() ? getTodayDate() : date,
            LOCAL_DATE_PATTERN
        );
        
        final String serviceResponseXML = privatesClient.getPrivates(idSession, parsedDate);
        final InvoicedServices invoicedServices = jaxbConversionUtils.convertStringXMLToObject(serviceResponseXML, InvoicedServices.class);
        
        final List<Integer> allDriverIds = vehicleRepository.getAllAuroVehicleIds();
        final List<TransferService> transferServices = invoicedServices.getTransferServices().stream()
            .filter(transferService -> allDriverIds.contains(transferService.getVehicleId()))
            .collect(Collectors.toList());
        
        return transferServices.stream()
            .map(transferService -> this.conversionService.convert(transferService, VtcTransfer.class))
            .collect(Collectors.toList());
    }
    
    private LocalDateTime getCurrentDateTimeWith3HoursMinus() {
        return LocalDateTime.now(ZONE_ID_PARIS).minusHours(3);
    }
    
    public String addTransfers(final List<TransferServiceData> transferServiceDataList) {
        final List<EVTCSERVICIOEMPRE> evtcservicioempreList =
            transferServiceDataList.stream()
                .map(transferServiceData ->
                    conversionService.convert(transferServiceData, EVTCSERVICIOEMPRE.class)
                ).collect(Collectors.toList());
        
        evtcservicioempreList.forEach(evtcservicioempre -> {
            try {
                final ABODYTYPERES abodytyperes = registerVTCClient.addTransfer(evtcservicioempre);
            } catch (final SOAPException_Exception e) {
                e.printStackTrace();
            }
        });
        
        return "";
    }
    
    private String getTodayDate() {
        return LocalDate.now(ZONE_ID_PARIS).format(DATE_TIME_FORMATTER);
    }
}
