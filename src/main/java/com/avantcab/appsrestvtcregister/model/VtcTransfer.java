package com.avantcab.appsrestvtcregister.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class VtcTransfer {
    
    private Long serviceId;
    private String carRegistration;
    private String holderNif;
    private String intermediaryNif;
    
    private String dateTimeContract;
    private String provinceContract;
    private String municipalityContract;
    
    private String provinceOrigin;
    private String municipalityOrigin;
    private String addressOrigin;
    
    private String startDateTime;
    private String provinceDestination;
    private String municipalityDestination;
    private String addressDestination;
    
    private String endDate;
    
    private String provinceMoreFar;
    private String municipalityMoreFar;
    private String addressMoreFar;
    
    private String trust;
}
