package com.avantcab.appsrestvtcregister.model.xml;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ServiciosFacturacion")
public class InvoicedServices {
    
    @XmlElement(name = "Servicio", required = true)
    private List<TransferService> transferServices;
    
    private InvoicedServices() {
    }
    
}
