package com.avantcab.appsrestvtcregister.model.xml.adapter;

import com.avantcab.appsrestvtcregister.model.xml.enums.ServiceType;
import com.avantcab.appsrestvtcregister.model.xml.pojo.ServiceAirportType;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Objects;

public class ServiceAirportTypeAdapter extends XmlAdapter<String, ServiceAirportType> {
    
    @Override
    public ServiceAirportType unmarshal(final String string) {
        final ServiceType existServiceType = ServiceType.fromValue(string);
        
        if (Objects.nonNull(existServiceType)) {
            return new ServiceAirportType(existServiceType);
        }
        
        return new ServiceAirportType(string);
    }
    
    @Override
    public String marshal(final ServiceAirportType serviceAirportType) {
        return serviceAirportType.getAirportCode() + StringUtils.SPACE +
            serviceAirportType.getServiceType().getValue();
    }
    
}
