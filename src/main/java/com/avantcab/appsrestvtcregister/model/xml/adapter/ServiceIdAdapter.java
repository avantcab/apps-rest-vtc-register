package com.avantcab.appsrestvtcregister.model.xml.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class ServiceIdAdapter extends XmlAdapter<String, Long> {

    @Override
    public Long unmarshal(final String serviceId) {
        return Long.valueOf(serviceId.replaceAll("[^0-9]", ""));
    }

    @Override
    public String marshal(final Long serviceId) {
        return serviceId.toString();
    }

}
