package com.avantcab.appsrestvtcregister.model.xml.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ServiceType {
    DEPARTURE("S"),
    ENTRANCE("E"),
    BETWEEN("SOLOT");

    private final String value;

    public static ServiceType fromValue(final String value) {
        ServiceType result = null;
        for (final ServiceType enumVal : ServiceType.values()) {
            if (enumVal.value.equals(value)) {
                result = enumVal;
                break;
            }
        }
        return result;
    }
}
