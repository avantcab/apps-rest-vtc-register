package com.avantcab.appsrestvtcregister.model.xml.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class LocalTimeAdapter extends XmlAdapter<String, LocalTime> {

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("H:mm");

    @Override
    public String marshal(final LocalTime time) {
        return time.format(dateFormat);
    }

    @Override
    public LocalTime unmarshal(final String time) {
        return LocalTime.parse(time, dateFormat);
    }

}
