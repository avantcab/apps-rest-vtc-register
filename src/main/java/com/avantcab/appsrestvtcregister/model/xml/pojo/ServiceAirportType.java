package com.avantcab.appsrestvtcregister.model.xml.pojo;

import com.avantcab.appsrestvtcregister.model.xml.enums.ServiceType;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public class ServiceAirportType {
    private final String airportCode;
    private final ServiceType serviceType;
    
    public ServiceAirportType(final String serviceTypeWithAirportCode) {
        this.airportCode = StringUtils.substringBefore(serviceTypeWithAirportCode, StringUtils.SPACE);
        this.serviceType = ServiceType.fromValue(
            StringUtils.substringAfter(serviceTypeWithAirportCode, StringUtils.SPACE)
        );
    }
    
    public ServiceAirportType(final ServiceType serviceType) {
        this.airportCode = "PMI";
        this.serviceType = serviceType;
    }
    
    public ServiceAirportType(final String airportCode, final ServiceType serviceType) {
        this.airportCode = airportCode;
        this.serviceType = serviceType;
    }
}
