package com.avantcab.appsrestvtcregister.model.xml;

import com.avantcab.appsrestvtcregister.model.xml.adapter.LocalDateAdapter;
import com.avantcab.appsrestvtcregister.model.xml.adapter.LocalTimeAdapter;
import com.avantcab.appsrestvtcregister.model.xml.adapter.ServiceAirportTypeAdapter;
import com.avantcab.appsrestvtcregister.model.xml.adapter.ServiceIdAdapter;
import com.avantcab.appsrestvtcregister.model.xml.pojo.ServiceAirportType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Servicio")
public class TransferService {
    
    @XmlElement(name = "Parte")
    @XmlJavaTypeAdapter(ServiceIdAdapter.class)
    private Long serviceId;
    
    @XmlElement(name = "CodigoServicio", required = true)
    @XmlJavaTypeAdapter(ServiceAirportTypeAdapter.class)
    private ServiceAirportType serviceAirportType;
    
    @XmlElement(name = "Fecha", required = true)
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate serviceDate;
    
    @XmlElement(name = "HoraInicio", required = true)
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    private LocalTime serviceTime;
    
    @XmlElement(name = "CodigoPresentacion", required = true)
    private String originPointCode;
    
    @XmlElement(name = "DescripcionPresentacion", required = true)
    private String originPointDescription;
    
    @XmlElement(name = "CodigoFinalizacion")
    private String destinationPointCode;
    
    @XmlElement(name = "DescripcionFinalizacion")
    private String destinationPointDescription;
    
    @XmlElement(name = "Referencia", required = true)
    private String referenceCode;
    
    @XmlElement(name = "CodigoVehiculo")
    private Integer vehicleId;
    
    private TransferService() {
    }
    
}
