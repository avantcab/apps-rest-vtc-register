package com.avantcab.appsrestvtcregister.model.xml.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Override
    public String marshal(final LocalDate date) {
        return date.format(dateFormat);
    }

    @Override
    public LocalDate unmarshal(final String date) {
        return LocalDate.parse(date, dateFormat);
    }

}
