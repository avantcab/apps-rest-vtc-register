package com.avantcab.appsrestvtcregister.model.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table(name = "service_id_vtc_register_id", schema = "vtc_register")
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class ServiceIdVtcRegisterId {
    
    @Id
    private Long serviceId;
    private Long vtcRegisterId;
    
}
