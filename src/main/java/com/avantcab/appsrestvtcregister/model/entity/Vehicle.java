package com.avantcab.appsrestvtcregister.model.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle", schema = "services")
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class Vehicle {
    
    @Id
    private int id;
    private int easyTaxiId;
    private String name;
    private String registration;
    
    @OneToOne
    private Business business;
    
}
