package com.avantcab.appsrestvtcregister.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TransferServiceData {
    
    private Long serviceId;
    private LocalDate serviceDate;
    private LocalTime serviceTime;
    private String originPointCode;
    private String originPointDescription;
    private String destinationPointCode;
    private String destinationPointDescription;
    private String referenceCode;
    private Integer vehicleId;
    
}
