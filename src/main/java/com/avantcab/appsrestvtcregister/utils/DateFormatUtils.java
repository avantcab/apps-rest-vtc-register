package com.avantcab.appsrestvtcregister.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateFormatUtils {
    
    public static final ZoneId ZONE_ID_PARIS = ZoneId.of("Europe/Paris");
    public static final String LOCAL_DATE_PATTERN = "yyyy-MM-dd";
    public static final String LOCAL_DATE_TIME_PATTERN = "yyyy-MM-dd H:mm";
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(LOCAL_DATE_PATTERN);
    
    public static LocalDateTime convertToLocalDateTime(final String dateTime, final String pattern) {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(dateTime, dateTimeFormatter);
    }
    
    public static String convertLocalDateTimeToString(final LocalDateTime localDateTime, final String pattern) {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        return localDateTime.format(dateTimeFormatter);
    }
    
    public static String convertLocalDateToString(final LocalDate localDate, final String pattern) {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        return localDate.format(dateTimeFormatter);
    }
    
    @SneakyThrows
    public static XMLGregorianCalendar convertDateToXMLGregorianCalendar(final String date, final String pattern) {
        final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        final GregorianCalendar gregorianCalendar = new GregorianCalendar();
        Date dateParsed;
        
        try {
            dateParsed = sdf.parse(date);
        } catch (final ParseException exp) {
            throw new RuntimeException(exp);
        }
        
        gregorianCalendar.setTime(dateParsed);
        
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
    }
    
}
