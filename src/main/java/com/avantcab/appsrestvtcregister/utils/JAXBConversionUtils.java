package com.avantcab.appsrestvtcregister.utils;

import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

@Component
public class JAXBConversionUtils {
    
    @SneakyThrows
    public <T> String convertObjectToStringXML(final T object) {
        final JAXBContext context = JAXBContext.newInstance(object.getClass());
        final Marshaller marshaller = context.createMarshaller();
        final StringWriter stringWriter = new StringWriter();
        
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(object, stringWriter);
        
        return stringWriter.toString();
    }
    
    
    public <T> T convertStringXMLToObject(final String xmlString, final Class<T> objectClass) {
        final JAXBContext jaxbContext;
        
        try {
            jaxbContext = JAXBContext.newInstance(objectClass);
            final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            final StringReader reader = new StringReader(xmlString);
            
            return (T) unmarshaller.unmarshal(reader);
        } catch (final JAXBException e) {
            throw new RuntimeException(e);
        }
    }
    
}
