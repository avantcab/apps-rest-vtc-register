package com.avantcab.appsrestvtcregister.controller;

import com.avantcab.appsrestvtcregister.model.entity.PointCodeZone;
import com.avantcab.appsrestvtcregister.service.PointCodeZoneService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/transfers")
public class PointCodeZoneController {
    
    private final PointCodeZoneService pointCodeZoneService;
    
    public PointCodeZoneController(final PointCodeZoneService pointCodeZoneService) {
        this.pointCodeZoneService = pointCodeZoneService;
    }
    
    @PostMapping(value = "/point-codes")
    public ResponseEntity<?> updateAllPointCodeZone(
        @RequestBody final List<PointCodeZone> pointCodeZones
    ) {
        final String result = pointCodeZoneService.updateAllPointCodeZone(pointCodeZones);
        return ResponseEntity.ok(result);
    }
    
}
