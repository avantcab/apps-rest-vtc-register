package com.avantcab.appsrestvtcregister.controller;

import com.avantcab.appsrestvtcregister.model.TransferServiceData;
import com.avantcab.appsrestvtcregister.model.VtcTransfer;
import com.avantcab.appsrestvtcregister.service.RegisterVTCService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/transfers")
public class RegisterVTCController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterVTCController.class);
    
    private final RegisterVTCService registerVTCService;
    
    public RegisterVTCController(final RegisterVTCService registerVTCService) {
        this.registerVTCService = registerVTCService;
    }
    
    @PostMapping(value = "/pending-register")
    public ResponseEntity<?> getPendingTransfersToRegister(
        @RequestBody final Map<String, Object> payload
    ) {
        final String date = payload.get("date").toString();
        LOGGER.info("Getting pending transfers to register in the VTC...");
        final List<VtcTransfer> vtcTransfers = registerVTCService.getPendingTransfersToRegister(date);
        return ResponseEntity.ok(vtcTransfers);
    }
    
    @PostMapping(value = "/pending-register-auro")
    public ResponseEntity<?> getPendingTransfersToRegisterAuro(
        @RequestBody final Map<String, Object> payload
    ) {
        final String date = payload.get("date").toString();
        LOGGER.info("Getting pending transfers to register in the VTC Auro...");
        final List<VtcTransfer> vtcTransfers = registerVTCService.getPendingTransfersToRegisterAuro(date);
        return ResponseEntity.ok(vtcTransfers);
    }
    
    @PostMapping(value = "/add")
    public ResponseEntity<?> addTransfers(
        @RequestBody final List<TransferServiceData> transferServiceDataList
    ) {
        final String result = registerVTCService.addTransfers(transferServiceDataList);
        return ResponseEntity.ok(HttpEntity.EMPTY);
    }
    
}
