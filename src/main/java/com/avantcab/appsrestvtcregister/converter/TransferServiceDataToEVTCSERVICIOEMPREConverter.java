package com.avantcab.appsrestvtcregister.converter;

import com.avantcab.appsrestvtcregister.converter.annotation.AutoRegisterConverter;
import com.avantcab.appsrestvtcregister.model.TransferServiceData;
import com.avantcab.appsrestvtcregister.model.entity.PointCodeZone;
import com.avantcab.appsrestvtcregister.model.entity.Vehicle;
import com.avantcab.appsrestvtcregister.repository.PointCodeZoneRepository;
import com.avantcab.appsrestvtcregister.repository.VehicleRepository;
import com.vtcregister.client.EVTCSERVICIOEMPRE;
import com.vtcregister.client.IDVERAZ;
import lombok.SneakyThrows;
import org.springframework.core.convert.converter.Converter;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.Temporal;
import java.util.GregorianCalendar;
import java.util.Optional;

@AutoRegisterConverter
public class TransferServiceDataToEVTCSERVICIOEMPREConverter implements Converter<TransferServiceData, EVTCSERVICIOEMPRE> {
    
    private final static String DEFAULT_LOCAL_DATE_TIME_FORMAT = "dd/MM/yyyy H:mm";
    private final static String DEFAULT_LOCAL_DATE_FORMAT = "dd/MM/yyyy";
    private final static String INTERMEDIARY_NIF = "B42811000";
    private final static String DEFAULT_PROVINCE = "07";
    private final static String DEFAULT_MUNICIPALITY = "040";
    
    private final PointCodeZoneRepository pointCodeZoneRepository;
    private final VehicleRepository vehicleRepository;
    
    public TransferServiceDataToEVTCSERVICIOEMPREConverter(
        final PointCodeZoneRepository pointCodeZoneRepository,
        final VehicleRepository vehicleRepository
    ) {
        this.pointCodeZoneRepository = pointCodeZoneRepository;
        this.vehicleRepository = vehicleRepository;
    }
    
    @Override
    @SneakyThrows
    public EVTCSERVICIOEMPRE convert(final TransferServiceData source) {
        final EVTCSERVICIOEMPRE evtcservicioempre = new EVTCSERVICIOEMPRE();
        final Optional<Vehicle> vehicleOptional = vehicleRepository.findVehicleById(source.getVehicleId());
        
        if (vehicleOptional.isPresent()) {
            final Vehicle vehicle = vehicleOptional.get();
            final StringBuilder carRegistration = new StringBuilder(vehicle.getRegistration());
            carRegistration.insert(3, "-");
            evtcservicioempre.setMatricula(carRegistration.toString().toUpperCase());
            evtcservicioempre.setNiftitular(vehicle.getBusiness().getCif());
        }
        
        evtcservicioempre.setNif(INTERMEDIARY_NIF);
        evtcservicioempre.setCgprovcontrato(DEFAULT_PROVINCE);
        evtcservicioempre.setCgmunicontrato(DEFAULT_MUNICIPALITY);
        evtcservicioempre.setFcontrato(convertToXMLGregorianCalendar(
            ZonedDateTime.now(ZoneId.of("ECT"))
        ));
        
        evtcservicioempre.setCgprovinicio(DEFAULT_PROVINCE);
        final Optional<PointCodeZone> origin =
            pointCodeZoneRepository.findPointCodeZoneByPointCode(source.getOriginPointCode());
        origin.ifPresent(
            pointCodeZone -> evtcservicioempre.setCgmuniinicio(pointCodeZone.getZoneIne().getIne())
        );
        evtcservicioempre.setDireccioninicio(source.getOriginPointDescription());
        
        final LocalDateTime startDateTime = source.getServiceDate().atTime(source.getServiceTime());
        evtcservicioempre.setFprevistainicio(convertToXMLGregorianCalendar(startDateTime));
        
        evtcservicioempre.setCgprovfin(DEFAULT_PROVINCE);
        final Optional<PointCodeZone> destination =
            pointCodeZoneRepository.findPointCodeZoneByPointCode(source.getDestinationPointCode());
        destination.ifPresent(
            pointCodeZone -> evtcservicioempre.setCgmunifin(pointCodeZone.getZoneIne().getIne())
        );
        evtcservicioempre.setDireccionfin(source.getDestinationPointDescription());
        
        evtcservicioempre.setFfin(convertToXMLGregorianCalendar(source.getServiceDate()));
        evtcservicioempre.setVeraz(IDVERAZ.S);
        
        return evtcservicioempre;
    }
    
    private String formatCarRegister(final String carRegistration) {
        final StringBuilder carRegistrationBuilder = new StringBuilder(carRegistration);
        return carRegistrationBuilder.insert(4, "-").toString();
    }
    
    private XMLGregorianCalendar convertToXMLGregorianCalendar(final Temporal localTemporal)
        throws DatatypeConfigurationException {
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(
            GregorianCalendar.from(ZonedDateTime.from(localTemporal))
        );
    }
}
