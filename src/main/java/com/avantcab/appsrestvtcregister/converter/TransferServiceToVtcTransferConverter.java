package com.avantcab.appsrestvtcregister.converter;

import com.avantcab.appsrestvtcregister.converter.annotation.AutoRegisterConverter;
import com.avantcab.appsrestvtcregister.model.VtcTransfer;
import com.avantcab.appsrestvtcregister.model.entity.PointCodeZone;
import com.avantcab.appsrestvtcregister.model.entity.Vehicle;
import com.avantcab.appsrestvtcregister.model.entity.ZoneIne;
import com.avantcab.appsrestvtcregister.model.xml.TransferService;
import com.avantcab.appsrestvtcregister.repository.PointCodeZoneRepository;
import com.avantcab.appsrestvtcregister.repository.VehicleRepository;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import static com.avantcab.appsrestvtcregister.utils.DateFormatUtils.ZONE_ID_PARIS;
import static com.avantcab.appsrestvtcregister.utils.DateFormatUtils.convertLocalDateTimeToString;
import static com.avantcab.appsrestvtcregister.utils.DateFormatUtils.convertLocalDateToString;

@AutoRegisterConverter
public class TransferServiceToVtcTransferConverter implements Converter<TransferService, VtcTransfer> {
    
    private final static String DEFAULT_LOCAL_DATE_TIME_FORMAT = "dd/MM/yyyy H:mm";
    private final static String DEFAULT_LOCAL_DATE_FORMAT = "dd/MM/yyyy";
    private final static String INTERMEDIARY_NIF = "B42811000";
    private final static String DEFAULT_PROVINCE = "07";
    private final static String DEFAULT_MUNICIPALITY = "040";
    
    private final PointCodeZoneRepository pointCodeZoneRepository;
    private final VehicleRepository vehicleRepository;
    
    public TransferServiceToVtcTransferConverter(
        final PointCodeZoneRepository pointCodeZoneRepository,
        final VehicleRepository vehicleRepository
    ) {
        this.pointCodeZoneRepository = pointCodeZoneRepository;
        this.vehicleRepository = vehicleRepository;
    }
    
    @Override
    public VtcTransfer convert(final TransferService transferService) {
        final Optional<Vehicle> vehicleOptional = vehicleRepository.findVehicleById(transferService.getVehicleId());
        
        final String carRegistration = vehicleOptional
            .map(value -> formatCarRegister(value.getRegistration()))
            .orElse("");
        final String holderNif = vehicleOptional
            .map(vehicle -> vehicle.getBusiness().getCif())
            .orElse("");
        
        final LocalDateTime localDateTimeContract = LocalDateTime.now(ZONE_ID_PARIS).minusDays(1);
        final LocalDateTime localDateTimeStart = transferService.getServiceDate()
            .atTime(transferService.getServiceTime());
        final LocalDate localDateEnd = transferService.getServiceDate();
        
        final Optional<PointCodeZone> originPointZone =
            pointCodeZoneRepository.findPointCodeZoneByPointCode(transferService.getOriginPointCode());
        final String origin = originPointZone
            .map(pointCodeZone -> {
                final ZoneIne zoneIne = pointCodeZone.getZoneIne();
                return zoneIne != null ? zoneIne.getIne() : "";
            })
            .orElse("");
        
        final Optional<PointCodeZone> destinationPointCode =
            pointCodeZoneRepository.findPointCodeZoneByPointCode(transferService.getDestinationPointCode());
        final String destination = destinationPointCode
            .map(pointCodeZone -> {
                final ZoneIne zoneIne = pointCodeZone.getZoneIne();
                return zoneIne != null ? zoneIne.getIne() : "";
            })
            .orElse("");
        
        return VtcTransfer.builder()
            .serviceId(transferService.getServiceId())
            .carRegistration(carRegistration)
            .holderNif(holderNif)
            .intermediaryNif(INTERMEDIARY_NIF)
            .dateTimeContract(convertLocalDateTimeToString(localDateTimeContract, DEFAULT_LOCAL_DATE_TIME_FORMAT))
            .provinceContract(DEFAULT_PROVINCE)
            .municipalityContract(DEFAULT_MUNICIPALITY)
            .provinceOrigin(DEFAULT_PROVINCE)
            .municipalityOrigin(origin)
            .addressOrigin(removeSpecialChar(transferService.getOriginPointDescription()))
            .startDateTime(convertLocalDateTimeToString(localDateTimeStart, DEFAULT_LOCAL_DATE_TIME_FORMAT))
            .provinceDestination(DEFAULT_PROVINCE)
            .municipalityDestination(destination)
            .addressDestination(removeSpecialChar(transferService.getDestinationPointDescription()))
            .endDate(convertLocalDateToString(localDateEnd, DEFAULT_LOCAL_DATE_FORMAT))
            .provinceMoreFar("")
            .municipalityMoreFar("")
            .addressMoreFar("")
            .trust("S")
            .build();
    }
    
    private String formatCarRegister(final String carRegistration) {
        final StringBuilder carRegistrationBuilder = new StringBuilder(carRegistration);
        return carRegistrationBuilder.insert(4, "-").toString();
    }
    
    private String removeSpecialChar(final String text) {
        return text.replaceAll(";", "");
    }
    
}
