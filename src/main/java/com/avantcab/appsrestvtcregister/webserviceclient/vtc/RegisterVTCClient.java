package com.avantcab.appsrestvtcregister.webserviceclient.vtc;

import com.vtcregister.client.ABODYTYPE;
import com.vtcregister.client.ABODYTYPERES;
import com.vtcregister.client.EVTCSERVICIOEMPRE;
import com.vtcregister.client.HEADERTYPE;
import com.vtcregister.client.Qaltavtc;
import com.vtcregister.client.Raltavtc;
import com.vtcregister.client.SOAPException_Exception;
import com.vtcregister.client.VTCPort;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.xml.datatype.DatatypeFactory;
import java.time.LocalDate;

@Component
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class RegisterVTCClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterVTCClient.class);
    private static final String HEADER_TYPE_VERSION = "1.0";
    
    private final VTCPort vtcPort;
    
    public ABODYTYPERES addTransfer(final EVTCSERVICIOEMPRE evtcservicioempre) throws SOAPException_Exception {
        final Qaltavtc request = new Qaltavtc();
        
        request.setHeader(buildHeaderType());
        request.setBody(buildBody(evtcservicioempre));
        
        final Raltavtc response = vtcPort.altaDeServicio(request);
        return response.getBody();
    }
    
    private ABODYTYPE buildBody(final EVTCSERVICIOEMPRE evtcservicioempre) {
        final ABODYTYPE abodytype = new ABODYTYPE();
        abodytype.setVtcservicio(evtcservicioempre);
        return abodytype;
    }
    
    @SneakyThrows
    private HEADERTYPE buildHeaderType() {
        final HEADERTYPE headertype = new HEADERTYPE();
        
        headertype.setFecha(
            DatatypeFactory
                .newInstance()
                .newXMLGregorianCalendar(LocalDate.now().toString())
        );
        headertype.setVersion(HEADER_TYPE_VERSION);
        headertype.setVersionsender(HEADER_TYPE_VERSION);
        
        return headertype;
    }
    
}
