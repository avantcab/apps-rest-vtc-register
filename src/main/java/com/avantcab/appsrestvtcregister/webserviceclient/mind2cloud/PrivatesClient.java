package com.avantcab.appsrestvtcregister.webserviceclient.mind2cloud;

import com.mind2cloudclient.IwsTransunion;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.xml.datatype.XMLGregorianCalendar;

@Component
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PrivatesClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrivatesClient.class);
    
    private final IwsTransunion wsTransunion;
    
    public String getPrivates(final String idSession, final XMLGregorianCalendar date) {
        LOGGER.info("Getting last privates (getPrivates)...");
        return wsTransunion.getPrivates(idSession, date);
    }
    
    public String getPrivatesByReference(final String idSession, final String reference) {
        LOGGER.info("Getting last privates by reference (getPrivatesByReference)...");
        final String response = wsTransunion.getPrivatesByReference(idSession, reference);
        LOGGER.info("GetPrivatesByReference Result: {}", response);
        return response;
    }
}
