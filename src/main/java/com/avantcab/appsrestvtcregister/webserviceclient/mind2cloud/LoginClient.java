package com.avantcab.appsrestvtcregister.webserviceclient.mind2cloud;

import com.mind2cloudclient.IwsTransunion;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Scope("singleton")
public class LoginClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginClient.class);
    
    private final IwsTransunion wsTransunion;
    
    private String idSession = "";
    
    @Value("${mind2cloud.client.username}")
    private String username;
    
    @Value("${mind2cloud.client.password}")
    private String password;
    
    public String getIdSession() {
        if (idSession.isEmpty()) {
            refreshIdSession();
        }
        return idSession;
    }
    
    @Scheduled(fixedRate = 200000)
    private void refreshIdSession() {
        idSession = this.checkLoginResponse(wsTransunion.login(username, password));
    }
    
    private String checkLoginResponse(final String response) {
        if (response.contains("Error login")) {
            LOGGER.error("Error trying to login to mind2cloud system!");
            throw new RuntimeException("Error trying to login to mind2cloud system!");
        }
        return response;
    }
    
}
