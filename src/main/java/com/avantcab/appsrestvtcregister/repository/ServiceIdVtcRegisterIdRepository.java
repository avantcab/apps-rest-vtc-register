package com.avantcab.appsrestvtcregister.repository;

import com.avantcab.appsrestvtcregister.model.entity.ServiceIdVtcRegisterId;
import com.avantcab.appsrestvtcregister.model.entity.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface ServiceIdVtcRegisterIdRepository extends CrudRepository<ServiceIdVtcRegisterId, String> {
    
    boolean existsByServiceId(final int serviceId);
    
}
