package com.avantcab.appsrestvtcregister.repository;

import com.avantcab.appsrestvtcregister.model.entity.Vehicle;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Integer> {
    
    @Query(value = "select v.id from Vehicle v where v.business.name LIKE '%AVANTCAB%'")
    List<Integer> getAllVehicleIds();
    
    @Query(value = "select v.id from Vehicle v where v.business.name LIKE '%AURO%'")
    List<Integer> getAllAuroVehicleIds();
    
    Optional<Vehicle> findVehicleById(final int vehicleId);
    
}
