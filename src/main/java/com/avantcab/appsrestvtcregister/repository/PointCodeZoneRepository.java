package com.avantcab.appsrestvtcregister.repository;

import com.avantcab.appsrestvtcregister.model.entity.PointCodeZone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PointCodeZoneRepository extends CrudRepository<PointCodeZone, String> {
    
    Optional<PointCodeZone> findPointCodeZoneByPointCode(final String pointCode);
    
}
