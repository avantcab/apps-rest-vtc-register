package com.avantcab.appsrestvtcregister.configuration;


import com.mind2cloudclient.IwsTransunion;
import com.mind2cloudclient.WsTransunion;
import com.vtcregister.client.VTCPort;
import com.vtcregister.client.VTCService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SoapClientConfig {
    
    @Bean
    public VTCPort vtcPort() {
        final VTCService vtcService = new VTCService();
        return vtcService.getVTCPortImplPort();
    }
    
    @Bean
    public IwsTransunion wsTransunion() {
        final WsTransunion wsTransunion = new WsTransunion();
        return wsTransunion.getBasicHttpBindingIwsTransunion();
    }
}
