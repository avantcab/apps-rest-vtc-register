package com.avantcab.appsrestvtcregister.configuration;

import com.avantcab.appsrestvtcregister.converter.annotation.AutoRegisterConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.DefaultConversionService;

@Configuration
public class ConversionServiceConfiguration implements SmartInitializingSingleton {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConversionServiceConfiguration.class);
    
    private final ListableBeanFactory beanFactory;
    private final DefaultConversionService conversionService;
    
    public ConversionServiceConfiguration(final ListableBeanFactory beanFactory) {
        this.beanFactory = beanFactory;
        this.conversionService = new DefaultConversionService();
    }
    
    @Bean
    public ConversionService conversionService() {
        return this.conversionService;
    }
    
    public void afterSingletonsInstantiated() {
        this.beanFactory.getBeansWithAnnotation(AutoRegisterConverter.class).forEach((entry, value) -> {
            this.registerConverter((Converter) value);
        });
    }
    
    private void registerConverter(final Converter converter) {
        this.conversionService.addConverter(converter);
        LOGGER.info("Registered converter {}", converter.getClass().getSimpleName());
    }
}
