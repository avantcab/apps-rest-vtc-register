
package com.mind2cloudclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.mind2cloud.client package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _LoginUsuario_QNAME = new QName("http://tempuri.org/", "Usuario");
    private final static QName _LoginPassword_QNAME = new QName("http://tempuri.org/", "Password");
    private final static QName _LoginResponseLoginResult_QNAME = new QName("http://tempuri.org/", "LoginResult");
    private final static QName _AddShuttlesIdSession_QNAME = new QName("http://tempuri.org/", "IdSession");
    private final static QName _AddShuttlesDocument_QNAME = new QName("http://tempuri.org/", "document");
    private final static QName _AddShuttlesResponseAddShuttlesResult_QNAME = new QName("http://tempuri.org/", "AddShuttlesResult");
    private final static QName _AddPrivatesResponseAddPrivatesResult_QNAME = new QName("http://tempuri.org/", "AddPrivatesResult");
    private final static QName _ConfirmShuttleSession_QNAME = new QName("http://tempuri.org/", "session");
    private final static QName _ConfirmShuttleResponseConfirmShuttleResult_QNAME = new QName("http://tempuri.org/", "ConfirmShuttleResult");
    private final static QName _ConfirmPrivateResponseConfirmPrivateResult_QNAME = new QName("http://tempuri.org/", "ConfirmPrivateResult");
    private final static QName _GetIncomingResponseGetIncomingResult_QNAME = new QName("http://tempuri.org/", "getIncomingResult");
    private final static QName _GetShuttleDeparturesResponseGetShuttleDeparturesResult_QNAME = new QName("http://tempuri.org/", "getShuttleDeparturesResult");
    private final static QName _GetShuttlesResponseGetShuttlesResult_QNAME = new QName("http://tempuri.org/", "getShuttlesResult");
    private final static QName _GetPrivatesResponseGetPrivatesResult_QNAME = new QName("http://tempuri.org/", "getPrivatesResult");
    private final static QName _GetPrivatesByReferenceReference_QNAME = new QName("http://tempuri.org/", "Reference");
    private final static QName _GetPrivatesByReferenceResponseGetPrivatesByReferenceResult_QNAME = new QName("http://tempuri.org/", "getPrivatesByReferenceResult");
    private final static QName _GetBookingResponseGetBookingResult_QNAME = new QName("http://tempuri.org/", "getBookingResult");
    private final static QName _ConfirmArriveShuttleResponseConfirmArriveShuttleResult_QNAME = new QName("http://tempuri.org/", "ConfirmArriveShuttleResult");
    private final static QName _GetPosVehiServRefServ_QNAME = new QName("http://tempuri.org/", "RefServ");
    private final static QName _GetPosVehiServResponseGetPosVehiServResult_QNAME = new QName("http://tempuri.org/", "getPosVehiServResult");
    private final static QName _GetPosVehiServEmtsResponseGetPosVehiServEmtsResult_QNAME = new QName("http://tempuri.org/", "getPosVehiServEmtsResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mind2cloud.client
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Login }
     *
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link LoginResponse }
     *
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link AddShuttles }
     *
     */
    public AddShuttles createAddShuttles() {
        return new AddShuttles();
    }

    /**
     * Create an instance of {@link AddShuttlesResponse }
     *
     */
    public AddShuttlesResponse createAddShuttlesResponse() {
        return new AddShuttlesResponse();
    }

    /**
     * Create an instance of {@link AddPrivates }
     *
     */
    public AddPrivates createAddPrivates() {
        return new AddPrivates();
    }

    /**
     * Create an instance of {@link AddPrivatesResponse }
     *
     */
    public AddPrivatesResponse createAddPrivatesResponse() {
        return new AddPrivatesResponse();
    }

    /**
     * Create an instance of {@link ConfirmShuttle }
     *
     */
    public ConfirmShuttle createConfirmShuttle() {
        return new ConfirmShuttle();
    }

    /**
     * Create an instance of {@link ConfirmShuttleResponse }
     *
     */
    public ConfirmShuttleResponse createConfirmShuttleResponse() {
        return new ConfirmShuttleResponse();
    }

    /**
     * Create an instance of {@link ConfirmPrivate }
     *
     */
    public ConfirmPrivate createConfirmPrivate() {
        return new ConfirmPrivate();
    }

    /**
     * Create an instance of {@link ConfirmPrivateResponse }
     *
     */
    public ConfirmPrivateResponse createConfirmPrivateResponse() {
        return new ConfirmPrivateResponse();
    }

    /**
     * Create an instance of {@link GetIncoming }
     *
     */
    public GetIncoming createGetIncoming() {
        return new GetIncoming();
    }

    /**
     * Create an instance of {@link GetIncomingResponse }
     *
     */
    public GetIncomingResponse createGetIncomingResponse() {
        return new GetIncomingResponse();
    }

    /**
     * Create an instance of {@link GetShuttleDepartures }
     *
     */
    public GetShuttleDepartures createGetShuttleDepartures() {
        return new GetShuttleDepartures();
    }

    /**
     * Create an instance of {@link GetShuttleDeparturesResponse }
     *
     */
    public GetShuttleDeparturesResponse createGetShuttleDeparturesResponse() {
        return new GetShuttleDeparturesResponse();
    }

    /**
     * Create an instance of {@link GetShuttles }
     *
     */
    public GetShuttles createGetShuttles() {
        return new GetShuttles();
    }

    /**
     * Create an instance of {@link GetShuttlesResponse }
     *
     */
    public GetShuttlesResponse createGetShuttlesResponse() {
        return new GetShuttlesResponse();
    }

    /**
     * Create an instance of {@link GetPrivates }
     *
     */
    public GetPrivates createGetPrivates() {
        return new GetPrivates();
    }

    /**
     * Create an instance of {@link GetPrivatesResponse }
     *
     */
    public GetPrivatesResponse createGetPrivatesResponse() {
        return new GetPrivatesResponse();
    }

    /**
     * Create an instance of {@link GetPrivatesByReference }
     *
     */
    public GetPrivatesByReference createGetPrivatesByReference() {
        return new GetPrivatesByReference();
    }

    /**
     * Create an instance of {@link GetPrivatesByReferenceResponse }
     *
     */
    public GetPrivatesByReferenceResponse createGetPrivatesByReferenceResponse() {
        return new GetPrivatesByReferenceResponse();
    }

    /**
     * Create an instance of {@link GetBooking }
     *
     */
    public GetBooking createGetBooking() {
        return new GetBooking();
    }

    /**
     * Create an instance of {@link GetBookingResponse }
     *
     */
    public GetBookingResponse createGetBookingResponse() {
        return new GetBookingResponse();
    }

    /**
     * Create an instance of {@link ConfirmArriveShuttle }
     *
     */
    public ConfirmArriveShuttle createConfirmArriveShuttle() {
        return new ConfirmArriveShuttle();
    }

    /**
     * Create an instance of {@link ConfirmArriveShuttleResponse }
     *
     */
    public ConfirmArriveShuttleResponse createConfirmArriveShuttleResponse() {
        return new ConfirmArriveShuttleResponse();
    }

    /**
     * Create an instance of {@link GetPosVehiServ }
     *
     */
    public GetPosVehiServ createGetPosVehiServ() {
        return new GetPosVehiServ();
    }

    /**
     * Create an instance of {@link GetPosVehiServResponse }
     *
     */
    public GetPosVehiServResponse createGetPosVehiServResponse() {
        return new GetPosVehiServResponse();
    }

    /**
     * Create an instance of {@link GetPosVehiServEmts }
     *
     */
    public GetPosVehiServEmts createGetPosVehiServEmts() {
        return new GetPosVehiServEmts();
    }

    /**
     * Create an instance of {@link GetPosVehiServEmtsResponse }
     *
     */
    public GetPosVehiServEmtsResponse createGetPosVehiServEmtsResponse() {
        return new GetPosVehiServEmtsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Double }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Float }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link QName }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Short }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Long }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "Usuario", scope = Login.class)
    public JAXBElement<String> createLoginUsuario(String value) {
        return new JAXBElement<String>(_LoginUsuario_QNAME, String.class, Login.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "Password", scope = Login.class)
    public JAXBElement<String> createLoginPassword(String value) {
        return new JAXBElement<String>(_LoginPassword_QNAME, String.class, Login.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "LoginResult", scope = LoginResponse.class)
    public JAXBElement<String> createLoginResponseLoginResult(String value) {
        return new JAXBElement<String>(_LoginResponseLoginResult_QNAME, String.class, LoginResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "IdSession", scope = AddShuttles.class)
    public JAXBElement<String> createAddShuttlesIdSession(String value) {
        return new JAXBElement<String>(_AddShuttlesIdSession_QNAME, String.class, AddShuttles.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "document", scope = AddShuttles.class)
    public JAXBElement<String> createAddShuttlesDocument(String value) {
        return new JAXBElement<String>(_AddShuttlesDocument_QNAME, String.class, AddShuttles.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "AddShuttlesResult", scope = AddShuttlesResponse.class)
    public JAXBElement<String> createAddShuttlesResponseAddShuttlesResult(String value) {
        return new JAXBElement<String>(_AddShuttlesResponseAddShuttlesResult_QNAME, String.class, AddShuttlesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "IdSession", scope = AddPrivates.class)
    public JAXBElement<String> createAddPrivatesIdSession(String value) {
        return new JAXBElement<String>(_AddShuttlesIdSession_QNAME, String.class, AddPrivates.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "document", scope = AddPrivates.class)
    public JAXBElement<String> createAddPrivatesDocument(String value) {
        return new JAXBElement<String>(_AddShuttlesDocument_QNAME, String.class, AddPrivates.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "AddPrivatesResult", scope = AddPrivatesResponse.class)
    public JAXBElement<String> createAddPrivatesResponseAddPrivatesResult(String value) {
        return new JAXBElement<String>(_AddPrivatesResponseAddPrivatesResult_QNAME, String.class, AddPrivatesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "session", scope = ConfirmShuttle.class)
    public JAXBElement<String> createConfirmShuttleSession(String value) {
        return new JAXBElement<String>(_ConfirmShuttleSession_QNAME, String.class, ConfirmShuttle.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ConfirmShuttleResult", scope = ConfirmShuttleResponse.class)
    public JAXBElement<String> createConfirmShuttleResponseConfirmShuttleResult(String value) {
        return new JAXBElement<String>(_ConfirmShuttleResponseConfirmShuttleResult_QNAME, String.class, ConfirmShuttleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "session", scope = ConfirmPrivate.class)
    public JAXBElement<String> createConfirmPrivateSession(String value) {
        return new JAXBElement<String>(_ConfirmShuttleSession_QNAME, String.class, ConfirmPrivate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ConfirmPrivateResult", scope = ConfirmPrivateResponse.class)
    public JAXBElement<String> createConfirmPrivateResponseConfirmPrivateResult(String value) {
        return new JAXBElement<String>(_ConfirmPrivateResponseConfirmPrivateResult_QNAME, String.class, ConfirmPrivateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "session", scope = GetIncoming.class)
    public JAXBElement<String> createGetIncomingSession(String value) {
        return new JAXBElement<String>(_ConfirmShuttleSession_QNAME, String.class, GetIncoming.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getIncomingResult", scope = GetIncomingResponse.class)
    public JAXBElement<String> createGetIncomingResponseGetIncomingResult(String value) {
        return new JAXBElement<String>(_GetIncomingResponseGetIncomingResult_QNAME, String.class, GetIncomingResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "session", scope = GetShuttleDepartures.class)
    public JAXBElement<String> createGetShuttleDeparturesSession(String value) {
        return new JAXBElement<String>(_ConfirmShuttleSession_QNAME, String.class, GetShuttleDepartures.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getShuttleDeparturesResult", scope = GetShuttleDeparturesResponse.class)
    public JAXBElement<String> createGetShuttleDeparturesResponseGetShuttleDeparturesResult(String value) {
        return new JAXBElement<String>(_GetShuttleDeparturesResponseGetShuttleDeparturesResult_QNAME, String.class, GetShuttleDeparturesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "IdSession", scope = GetShuttles.class)
    public JAXBElement<String> createGetShuttlesIdSession(String value) {
        return new JAXBElement<String>(_AddShuttlesIdSession_QNAME, String.class, GetShuttles.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getShuttlesResult", scope = GetShuttlesResponse.class)
    public JAXBElement<String> createGetShuttlesResponseGetShuttlesResult(String value) {
        return new JAXBElement<String>(_GetShuttlesResponseGetShuttlesResult_QNAME, String.class, GetShuttlesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "IdSession", scope = GetPrivates.class)
    public JAXBElement<String> createGetPrivatesIdSession(String value) {
        return new JAXBElement<String>(_AddShuttlesIdSession_QNAME, String.class, GetPrivates.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getPrivatesResult", scope = GetPrivatesResponse.class)
    public JAXBElement<String> createGetPrivatesResponseGetPrivatesResult(String value) {
        return new JAXBElement<String>(_GetPrivatesResponseGetPrivatesResult_QNAME, String.class, GetPrivatesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "session", scope = GetPrivatesByReference.class)
    public JAXBElement<String> createGetPrivatesByReferenceSession(String value) {
        return new JAXBElement<String>(_ConfirmShuttleSession_QNAME, String.class, GetPrivatesByReference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "Reference", scope = GetPrivatesByReference.class)
    public JAXBElement<String> createGetPrivatesByReferenceReference(String value) {
        return new JAXBElement<String>(_GetPrivatesByReferenceReference_QNAME, String.class, GetPrivatesByReference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getPrivatesByReferenceResult", scope = GetPrivatesByReferenceResponse.class)
    public JAXBElement<String> createGetPrivatesByReferenceResponseGetPrivatesByReferenceResult(String value) {
        return new JAXBElement<String>(_GetPrivatesByReferenceResponseGetPrivatesByReferenceResult_QNAME, String.class, GetPrivatesByReferenceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "IdSession", scope = GetBooking.class)
    public JAXBElement<String> createGetBookingIdSession(String value) {
        return new JAXBElement<String>(_AddShuttlesIdSession_QNAME, String.class, GetBooking.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "Reference", scope = GetBooking.class)
    public JAXBElement<String> createGetBookingReference(String value) {
        return new JAXBElement<String>(_GetPrivatesByReferenceReference_QNAME, String.class, GetBooking.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getBookingResult", scope = GetBookingResponse.class)
    public JAXBElement<String> createGetBookingResponseGetBookingResult(String value) {
        return new JAXBElement<String>(_GetBookingResponseGetBookingResult_QNAME, String.class, GetBookingResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "IdSession", scope = ConfirmArriveShuttle.class)
    public JAXBElement<String> createConfirmArriveShuttleIdSession(String value) {
        return new JAXBElement<String>(_AddShuttlesIdSession_QNAME, String.class, ConfirmArriveShuttle.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "Reference", scope = ConfirmArriveShuttle.class)
    public JAXBElement<String> createConfirmArriveShuttleReference(String value) {
        return new JAXBElement<String>(_GetPrivatesByReferenceReference_QNAME, String.class, ConfirmArriveShuttle.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ConfirmArriveShuttleResult", scope = ConfirmArriveShuttleResponse.class)
    public JAXBElement<String> createConfirmArriveShuttleResponseConfirmArriveShuttleResult(String value) {
        return new JAXBElement<String>(_ConfirmArriveShuttleResponseConfirmArriveShuttleResult_QNAME, String.class, ConfirmArriveShuttleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "IdSession", scope = GetPosVehiServ.class)
    public JAXBElement<String> createGetPosVehiServIdSession(String value) {
        return new JAXBElement<String>(_AddShuttlesIdSession_QNAME, String.class, GetPosVehiServ.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "RefServ", scope = GetPosVehiServ.class)
    public JAXBElement<String> createGetPosVehiServRefServ(String value) {
        return new JAXBElement<String>(_GetPosVehiServRefServ_QNAME, String.class, GetPosVehiServ.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getPosVehiServResult", scope = GetPosVehiServResponse.class)
    public JAXBElement<String> createGetPosVehiServResponseGetPosVehiServResult(String value) {
        return new JAXBElement<String>(_GetPosVehiServResponseGetPosVehiServResult_QNAME, String.class, GetPosVehiServResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "IdSession", scope = GetPosVehiServEmts.class)
    public JAXBElement<String> createGetPosVehiServEmtsIdSession(String value) {
        return new JAXBElement<String>(_AddShuttlesIdSession_QNAME, String.class, GetPosVehiServEmts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "RefServ", scope = GetPosVehiServEmts.class)
    public JAXBElement<String> createGetPosVehiServEmtsRefServ(String value) {
        return new JAXBElement<String>(_GetPosVehiServRefServ_QNAME, String.class, GetPosVehiServEmts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getPosVehiServEmtsResult", scope = GetPosVehiServEmtsResponse.class)
    public JAXBElement<String> createGetPosVehiServEmtsResponseGetPosVehiServEmtsResult(String value) {
        return new JAXBElement<String>(_GetPosVehiServEmtsResponseGetPosVehiServEmtsResult_QNAME, String.class, GetPosVehiServEmtsResponse.class, value);
    }

}
